from misc.utils import *
import pickle

Gmag = scipy.io.loadmat('../data/histv3/magnitude.mat')
Gdir = scipy.io.loadmat('../data/histv3/direction.mat')
jointhistogram = scipy.io.loadmat('../data/histv3/JointHistogram.mat')
pyramidalhistogram = scipy.io.loadmat('../data/histv3/PyramidalHistogram.mat')
divcurlhistogram = scipy.io.loadmat('../data/histv3/DivCurlHistogram.mat')
pyramidalvector = scipy.io.loadmat('../data/histv3/PyramidalVectorHistogram.mat')
listfeatures = {'gmag':Gmag, 'gdir':Gdir, 'joint': jointhistogram, 'pyramidal': pyramidalhistogram,\
                'pvector': pyramidalvector, 'divcurl': divcurlhistogram}
features = listfeatures['gmag']['listfeatures']
y = np.asarray([x[1][0][0] for x in features])
testlabels = y[404:584]
trainlabels=np.append(y[:404], y[584:])
skf = cross_validation.StratifiedKFold(trainlabels, n_folds=5)
train_indices=[]
test_indices=[]
for train_index, test_index in skf:
    y_train = trainlabels[train_index]
    y_train, y_index = oversample_label(y_train, train_index)
    train_indices.append(y_index)
    test_indices.append(test_index)
custom_cv=zip(train_indices, test_indices)


Cparameters = []
for a in range(-12,12):
    Cparameters.append(2 ** a)
Gammaparameters = []
for a in range(-12,12):
    Gammaparameters.append(2 ** a)
    
tuned_parameters = [{'kernel': ['rbf'], 'gamma': Gammaparameters,
                     'C': Cparameters},
                    {'kernel': ['linear'], 'C': Cparameters}]

features = listfeatures['pyramidal']['listfeatures']
Xpyr = np.asarray([x[0][0] for x in features])
trainXpyr = np.append(Xpyr[:404], Xpyr[584:],axis=0)
testXpyr = Xpyr[404:584]
scaler = StandardScaler()
X = scaler.fit_transform(trainXpyr)
clfpyr = grid_search.GridSearchCV(estimator=svm.SVC(), param_grid=tuned_parameters, n_jobs=-1, cv=custom_cv, scoring=make_scorer(get_average_precision))
clfpyr.fit(X, trainlabels)

features = listfeatures['divcurl']['listfeatures']
Xdivcurl = np.asarray([x[0][0] for x in features])
trainXdivcurl = np.append(Xdivcurl[:404], Xdivcurl[584:],axis=0)
testXdivcurl = Xdivcurl[404:584]
scaler = StandardScaler()
X = scaler.fit_transform(trainXdivcurl)
clfdivcurl = grid_search.GridSearchCV(estimator=svm.SVC(), param_grid=tuned_parameters, n_jobs=-1, cv=custom_cv, scoring=make_scorer(get_average_precision))
clfdivcurl.fit(X, trainlabels)

features = listfeatures['pvector']['listfeatures']
Xpvector = np.asarray([x[0][0] for x in features])
trainXpvector = np.append(Xpvector[:404], Xpvector[584:],axis=0)
testXpvector = Xpvector[404:584]
scaler = StandardScaler()
X = scaler.fit_transform(trainXpvector)
clfpvector = grid_search.GridSearchCV(estimator=svm.SVC(), param_grid=tuned_parameters, n_jobs=-1, cv=custom_cv, scoring=make_scorer(get_average_precision))
clfpvector.fit(X, trainlabels)

listclf = [clfpyr, clfdivcurl, clfpvector]
PIK="clf.dat"
with open(PIK, "wb") as f:
    pickle.dump(listclf, f)

