import numpy as np
from sklearn.cross_validation import StratifiedKFold
import sklearn.metrics as metrics
import scipy.io
import h5py
from sklearn import svm, grid_search
from sklearn import cross_validation, grid_search
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import make_scorer
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler

from IPython.display import display, Image
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib
import pandas as pd

import base64
import skimage.io

class Image(skimage.io.Image):
    def __init__(self, *args, **kwargs):
        self._width = kwargs.pop('width', '100%')
        skimage.io.Image.__init__(self, *args, **kwargs)
    def _repr_html_(self):
        out = '<img width="%s"' % self._width
        out += ' src="data:image/png;base64,' + \
               base64.b64encode(np.compat.asbytes(self._repr_png_())) + '"/>'
        return out


class side_by_side():
    def __init__(self, *frames):
        self.frames = frames

    def _repr_html_(self):
        s = ""
        for f in self.frames:
            s += "<div style='float: left;'>" + f._repr_html_() + "</div>"

        return s
def all_same(items):
    return all(x == items[0] for x in items)
def get_samples(features):
    X = np.asarray([x[0][0] for x in features])
    label = np.asarray([x[1][0][0] for x in features])
    return X, label

def oversample_label(y, y_index):
    counts = np.bincount(y)
    classes = np.unique(y)
    counts = counts[1:]
    maxvalue = counts[counts.argmax()]

    copydf = y.copy()
    #print counts, classes, counts.argmax(), maxvalue, len(copydf)
    while not all_same(counts):
        minvalue = classes[counts.argmin()]
        tmp = copydf[copydf == minvalue]
        tmp_index = y_index[copydf == minvalue]
        #print tmp_index
        xrandom = np.random.randint(len(tmp))
        y = np.append(y, tmp[xrandom])
        y_index = np.append(y_index, tmp_index[xrandom])
        counts = np.bincount(y)
        counts = counts[1:]
        #print counts
    return y, y_index
def get_average_precision(y_true, y_scores, printable=False):
    cm = confusion_matrix(y_true, y_scores)
    cm_normalized = []
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    a = np.mean(np.diag(cm_normalized))
    if printable:
        print cm_normalized
    return a

from matplotlib.colors import Normalize
class MidpointNormalize(Normalize):

    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))
