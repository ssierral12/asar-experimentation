function [imgC, imgMC] = get_samples(tImage, wImage, anotaciones, x, y, oversampling, threshold, aclass, volumen)
%anotaciones contiene las confluencias, divergencias y vorticidades
%anotaciones{1} da acceso a vorticidades {2} a divergencias y {3} a confluencias
%Ademas las anotaciones deben ser solo de este tiempo y nivel determinado
%0 in oversampling means no oversampling :P
%img** tendrá la siguiente forma: es un cell array así {wImage class}, donde C1, C2 y C3 corresponde a anotaciones 1/0 y MC a 1/2/3
z = size(tImage)
imgcounter = 1
imgC{1} = {'Imagen' 'Clase'}
imgMC{1} = {'Imagen' 'Clase'}
for k=1:(floor(z(2)/oversampling)-1)
	for j=1:(floor(z(1)/oversampling)-1)
		x1 = (k-1)*oversampling+1;
		x2 = (k-1)*oversampling + x;
		y1 = (j-1)*oversampling+1;
		y2 = (j-1)*oversampling + y;
		temp = tImage(y1:y2, x1:x2);
		wind = wImage(y1:y2, x1:x2,:);
		
		sample = [x1, x2, y1, y2];
		anots = anotaciones;
		switch aclass
		case 'vortices'
			%imgC{1} = {'Imagen' 'Clase'}
			%imgMC{1} = {'Imagen' 'Clase'}
			if get_threshold(sample, anots, threshold, volumen) == true
				imgC{end+1} = {wImage(y1:y2, x1:x2,:) 1}
				imgMC{end+1} = {wImage(y1:y2, x1:x2,:) 1}
			else
				imgC{end+1} = {wImage(y1:y2, x1:x2,:) 0};
			end
		case 'divergencias'
			%imgC{1} = {'Imagen' 'Clase'}
			%imgMC{1} = {'Imagen' 'Clase'}
			if get_threshold(sample, anots, threshold, volumen) == true
				imgC{end+1} = {wImage(y1:y2, x1:x2,:) 1}
				imgMC{end+1} = {wImage(y1:y2, x1:x2,:) 2}
			else
				imgC{end+1} = {wImage(y1:y2, x1:x2,:) 0};
			end
		case 'confluencias'
			%imgC{1} = {'Imagen' 'Clase'}
			%imgMC{1} = {'Imagen' 'Clase'}
			if get_threshold(sample, anots, threshold, volumen) == true
				imgC{end+1} = {wImage(y1:y2, x1:x2,:) 1}
				imgMC{end+1} = {wImage(y1:y2, x1:x2,:) 3}
			else
				imgC{end+1} = {wImage(y1:y2, x1:x2,:) 0};
			end
		end
	end
end
%if in case the size does not divide exactly we had to set a proper boundary(redefining oversampling)
end


%anot = [x1, x2, y1, y2] and so is sample
function positive = get_threshold(sample, anots, threshold, volumen)
	%samples contiene las anotaciones de esa clase y example contiene la muestra que 
	z = size(anots);
	positive = 0
	for k=1:z(2)
	anot = [get_pixel(anots(k).x1, volumen(1).xmax, volumen(1).xmin, volumen(1).splits) get_pixel(anots(k).x2, volumen(1).xmax, volumen(1).xmin, volumen(1).splits) get_pixel(anots(k).y1, volumen(2).xmax, volumen(2).xmin, volumen(2).splits) get_pixel(anots(k).y2, volumen(2).xmax, volumen(2).xmin, volumen(2).splits)];
	si = max(0, min(anot(2), sample(2)) - max(anot(1), sample(1))) * max(0, min(anot(4), sample(4)) - max(anot(3), sample(3)));
	sanot = (max(anot(1),anot(2))-min(anot(1),anot(2)))*(max(anot(3),anot(4))-min(anot(3),anot(4)));
	value = si/sanot;
	if value >= threshold
		positive = 1
		return
	end
	end
end

function pixel = get_pixel(xtmp, xmax, xmin, splits)
	pixel = floor(((xtmp-xmin)*splits)/(xmax-xmin));
end
