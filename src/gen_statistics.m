function [statistics, data] = gen_statistics(annotations, volumen)
	%annotations es un cell array conteniendo en 1 Vorticidad, 2 Divergencia y 3 Confluencia
	z=size(annotations);
	data{1}={'x1' 'x2' 'y1' 'y2'};
	for k=1:z(2)
		a=size(annotations{k})
		tmpano = annotations{k};
		for j=1:a(2)
			x1tmp = tmpano(j).x1;
			x2tmp = tmpano(j).x2;
			y1tmp = tmpano(j).y1;
			y2tmp = tmpano(j).y2;
			disp(sprintf('Clase %d anotación %d', k,  j));
			data{end+1}= [get_pixel(x1tmp, volumen(1).xmax, volumen(1).xmin, volumen(1).splits) get_pixel(x2tmp, volumen(1).xmax, volumen(1).xmin, volumen(1).splits) get_pixel(y1tmp, volumen(2).xmax, volumen(2).xmin, volumen(2).splits) get_pixel(y2tmp, volumen(2).xmax, volumen(2).xmin, volumen(2).splits)];
		end
	end
	b = size(data);
	x = zeros(b(2)-1, 2);
	for l=2:b(2)
		x(l-1,1)=max(data{l}(1), data{l}(2)) - min(data{l}(1), data{l}(2));
		x(l-1,2)=max(data{l}(3), data{l}(4)) - min(data{l}(3), data{l}(4));
	end
	statistics(1).mean = [mean(x(:,1)) mean(x(:,2))];
	statistics(1).std = [std(x(:,1)) std(x(:,2))];
	statistics(1).min = [min(x(:,1)) min(x(:,2))];
	statistics(1).max = [max(x(:,1)) max(x(:,2))];
end

function pixel = get_pixel(xtmp, xmax, xmin, splits)
	pixel = floor(((xtmp-xmin)*splits)/(xmax-xmin));
end
