function imgMC = getMulticlass(wImage, anotaciones, x, y, volumen)
	z = size(anotaciones);
	x1=get_pixel(anotaciones.x1, volumen(1).xmax, volumen(1).xmin, volumen(1).splits);
	x2=get_pixel(anotaciones.x2, volumen(1).xmax, volumen(1).xmin, volumen(1).splits);
	y1=get_pixel(anotaciones.y1, volumen(2).xmax, volumen(2).xmin, volumen(2).splits);
	y2=get_pixel(anotaciones.y2, volumen(2).xmax, volumen(2).xmin, volumen(2).splits);
	centerx = floor((x1 + x2)/2);
	centery = floor((y1 + y2)/2);
	if (centerx + x/2) > volumen(1).splits
		centerx = volumen(1).splits - x/2;
	elseif (centerx - x/2) < 1
		centerx = x/2 + 1;
	end
	if (centery + y/2) > volumen(2).splits
		centery = volumen(2).splits - y/2;
	elseif (centery - y/2) < 1
		centery = y/2 + 1;
	end
	defy = [floor(centery-y/2) floor(centery+y/2)];
	defx = [floor(centerx-x/2) floor(centerx+x/2)];
	imgMC = wImage(defy(1):defy(2), defx(1):defx(2),:);
end

function pixel = get_pixel(xtmp, xmax, xmin, splits)
	pixel = floor(((xtmp-xmin)*splits)/(xmax-xmin));
end
