function anotaciones = get_anotaciones(marcas)
if marcas.getLength == 0
anotaciones = struct;
return
end
for j = 0:marcas.getLength-1
marca = marcas.item(j);
idmarca = char(marca.getAttribute('id'));
%disp(sprintf('"%s"', idmarca));
nivelmarca = char(marca.getAttribute('nivel'));
%disp(sprintf('"%s"', nivelmarca));
puntos = marca.getElementsByTagName('punto');
x1= char(puntos.item(0).getAttribute('x'));
x2= char(puntos.item(1).getAttribute('x'));
y1= char(puntos.item(0).getAttribute('y'));
y2= char(puntos.item(1).getAttribute('y'));
%disp(sprintf('"%s", "%s", "%s", "%s"', x1, x2, y1, y2));
index = j + 1;
anotaciones(index).tiempo = str2num(marca.getAttribute('tiempo'));
anotaciones(index).nivel = round(str2double(marca.getAttribute('nivel'))*10000)/10000.0;
anotaciones(index).x1 = str2num(x1);
anotaciones(index).x2 = str2num(x2);
anotaciones(index).y1 = str2num(y1);
anotaciones(index).y2 = str2num(y2);
end
end

