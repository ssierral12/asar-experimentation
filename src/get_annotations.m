function [vortices, divergencias, confluencias] = get_annotations(filename)
%filename = '2004-04-29_00-00-00_climate_2014-09-29-JuanMolina-jmolinar3@ucentral.edu.co.wsa';
xDoc = xmlread(filename);
allListitems = xDoc.getElementsByTagName('estructura');

%Trabajar con String de Java
%annotations = {cell(1,6); cell(1,6); cell(1,6)}
%tmpindex = 0;
for k = 0:allListitems.getLength-1
estructura = allListitems.item(k);
marcas = estructura.getElementsByTagName('marca');
nombreEstructura = char(estructura.getAttribute('nombre'));
%disp(sprintf('"%s"', nombreEstructura));
switch nombreEstructura
	case 'Vorticidad'
		vortices = get_anotaciones(marcas);
	case 'Divergencia'
		divergencias = get_anotaciones(marcas);
	case 'Confluencia'
		confluencias = get_anotaciones(marcas);
	otherwise
		warning('Las anotaciones son incorrectas')
end
%tmpindex = tmpindex + marcas.getLength;
%clearvars estructura marcas
end
end
