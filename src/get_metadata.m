function metadata = get_metadata(filename, type)
xDoc = xmlread(filename);
data = xDoc.getElementsByTagName(type);
valores = data.item(0).getElementsByTagName('data').item(0);
charvalor = char(valores.getTextContent);
%disp(sprintf('"%s"', charvalor));
metadata = regexp(charvalor, '[-+]?[0-9]*\.?[0-9]*', 'match')
metadata = str2num(char(metadata));
metadata = round(metadata*10000)/10000.0;
end
