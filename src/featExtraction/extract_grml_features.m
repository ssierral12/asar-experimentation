function v = extract_grml_features(img)
%extract run-length features

GRLM = grayrlmatrix(img);
v = grayrlprops(GRLM);
v = mean(v,1); 

%debug
% figure,hold on; title('Run-length');
% for i=1:length(GRLM)
%     v = GRLM{i};
%     subplot(1,length(GRLM),i),imshow(mat2gray(v));
% end

end