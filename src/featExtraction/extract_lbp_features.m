function [Is] = extract_lbp_features(image, Rs, Ss, verbose)

if size(image,3)==3
    image=rgb2gray(image);
end 

%histogram map
%mapping8=getmapping(8,'riu2'); 
%Ms(length(Ss)) = getmapping(2,'riu2');
%for s = unique(Ss)
%	m = getmapping(s,'riu2');
%	Ms(find(Ss==s))=m;
%end

Is={};
for i = 1:length(Rs)
	%temp = lbp(image,Rs(i),Ss(i),0,'i');
    [temp] = multibit_lbp(image,uint8(Rs(i)),Ss(i));
    Is{i} = temp;
	%fprintf('Made %d radius image\n',i);
end

%% Trim small samples
% base = find(Rs==max(Rs));
% for i = 1:length(Is)
% 	if(i == base)
% 		continue;
% 	end
% 	temp = Is{i};
% 	mod = Rs(base)-Rs(i);
% 	temp = temp(mod+1:size(temp,1)-mod,mod+1:size(temp,2)-mod,:);
% 	Is{i} = temp;
% end
% 
% %% size augmention
% Is2 = cell(size(Is));
% for i = 1:length(Is)
%     type = class(Is{i});
% 	temp = zeros(size(image,1),size(image,2),size(temp,3));
%     temp = cast(temp,type);
%     temp(Rs(base)+1:size(temp,1)-Rs(base),Rs(base)+1:size(temp,2)-Rs(base),:) = Is{i};
%     Is2{i} = temp;
% end
% Is = Is2;

%multi-scale distances
% Is = Is(Rs(base)+1:size(Is,1)-Rs(base)-1,Rs(base)+1:size(Is,2)-Rs(base)-1,:);
% vec = reshape(Is,[size(Is,1)*size(Is,2),size(Is,3)]);
% distances = reshape(distances,[size(Is,1),size(Is,2)]);
% dist_map = zeros();
% dist_map(Rs(base):size(distances,1)+Rs(base),Rs(base):size(distance,2)+Rs(base),:);

%size(matches)/(size(Is,1)*size(Is,2));
% if verbose
%     figure;
%     org = new_img(x-Rs(base):x+Rs(base),y-Rs(base):y+Rs(base),:);
%     name = sprintf('%s/original.png',folder)
%     imwrite(org,name,'png')
% 
%     for i = 1:length(matches)
%         out = new_img(matches(i,1)-Rs(base):matches(i,1)+Rs(base),matches(i,2)-Rs(base):matches(i,2)+Rs(base),:);
%         name = sprintf('%s/match_%04d.png',folder,i)
%         imwrite(out,name,'png')
%     end
%     
%     close all;
%     hold off
%     imagesc(new_img)
%     set(gca,'visible','off');
%     hold on
%     print('-dpng','-r600',[folder '/org_img.png'])
%     hold off
%     close all;
%     imagesc(new_img)
%     set(gca,'visible','off');
%     hold on
% 
%     scatter(matches(:,2),matches(:,1),'LineWidth',1,'MarkerEdgeColor','y')
%     scatter(y,x,'LineWidth',1,'MarkerEdgeColor','g')
%     print('-dpng','-r600',[folder '/pos_found.png'])
%     close all
% end 

end