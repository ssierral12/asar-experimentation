function vect = extract_haralick_features(img,mask)

vect = [];
v = haralick_img(img,mask,256,3,1,true);
for vi=1:size(v.img3,3)
  tmp = v.img3(:,:,vi);
  nonnan = find(~isnan(tmp(:)));%discard NaN errors
  if ~isempty(nonnan)
    mt = mean(tmp(nonnan)); stdt=std(tmp(nonnan));
  else
    mt = 0; stdt = 0;
  end
  vect = [vect mt stdt];  
end

end