function listfeatures = getFeatures(copyroute, featureName)
	label = [];
	listfeatures = cell(size(copyroute,2),2);
	index = 1;
	for x = copyroute
		img = imread(fullfile('/home','ssierral', char(x{1}(1))));
		size(img);
		disp(fullfile('/home','ssierral', char(x{1}(1))))
		feature = CCIPD_getFeature(featureName, img);
		label = [label cell2mat(x{1}(2))];
		listfeatures{index, 1} = feature;
		listfeatures{index, 2} = cell2mat(x{1}(2));
		index = index+1;
	end
	save(sprintf('%s.mat', featureName), 'listfeatures')
end
