clc;clear all;close all;
addpath('haralick')
addpath('Gabor_filter')

%filename='imgMC';
%load(filename);
%copyimg = imgMC(:,2:end)
%for x = copyimg
%	img = cell2mat(x{1}(1))
%	feature = CCIPD_getFeature('HaralickFeatures',img)
%end

filename='imgroute';
load(filename);
copyroute = imgroute(:,2:end);

funList = {@getFeatures,@getFeatures}
feaList = {'HaralickFeatures','GaborFeatures'}

%matlabpool open

%parfor i=1:length(funList)
%	funList{i}(copyroute, feaList{i});
%end

listfeatures = getFeatures(copyroute, 'GaborFeatures');

