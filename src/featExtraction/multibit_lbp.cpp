#include "mex.h"
#include "matrix.h"
#include <stdint.h>
#include <math.h>

#define TRUE 1
#define FALSE 0

const double pi = 3.1415926;

typedef unsigned char uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

inline void sub2ind(int rc, int &index, int r, int c)
{
    index = c*rc + r;
}
//Currently only 8-bits is supported
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{	
	if(nlhs != 1)
        mexErrMsgTxt("map = multibit_lbp(img,Rs,Ss)");
    if(mxGetNumberOfDimensions(prhs[0]) > 2)
		mexErrMsgTxt("First argument 'img' must be 2D (i.e. grayscale image).");
	if(!mxIsClass(prhs[0],"uint8"))
		mexErrMsgTxt("First argument 'img' must be of class 'uint8'.");
   
    double **spoints;
    uint8 neighbors;
    if (nrhs==1)
    {
       spoints = new double*[8];
       for(int i=0; i<8; i++)
           spoints[i] = new double[2];
       spoints[0][0]=-1;spoints[0][1]=-1;spoints[1][0]=-1;spoints[1][1]= 0;
       spoints[2][0]=-1;spoints[2][0]= 1;spoints[3][0]= 0;spoints[3][1]=-1;
       spoints[4][0]=-0;spoints[4][1]= 1;spoints[5][0]= 1;spoints[5][1]=-1;
       spoints[6][0]= 1;spoints[6][1]= 0;spoints[7][0]= 1;spoints[7][0]= 1;
       neighbors = 8;
    }
    else if (nrhs==3)
    {
        neighbors = *(uint8 *)mxGetData(prhs[1]);
        double *radius = (double *)mxGetData(prhs[2]);
        spoints = new double*[neighbors];
        for(int i=0; i<neighbors; i++)
           spoints[i] = new double[2];
        
        double a = (2.0*pi)/neighbors;
        for(int i=0; i<neighbors; i++)
        {
          spoints[i][0] = -(*radius)*sin(i*a);
          spoints[i][1] = (*radius)*cos(i*a);
        }
    }
    else
    {
        mexErrMsgTxt("lbpim must be lbp_c(img,radius,bits_length)");
    }
    
    int nRow = mxGetM(prhs[0]);
    int nCol = mxGetN(prhs[0]); 
    int dims[3];
    dims[0] = nRow; dims[1] = nCol; dims[2] = neighbors/8;
    uint8 *img = (uint8*)mxGetData(prhs[0]);
    plhs[0] = mxCreateNumericArray(3,dims,mxUINT8_CLASS,mxREAL);
    uint8 *bnd = (uint8*)mxGetData(plhs[0]);
    uint8 sumNeighbors = 0;
        
    int index,cur_pixel,x,y,fx,fy,cx,cy,rx,ry,tx,ty,w1,w2,w3,w4,N,ffindex,fcindex,cfindex,ccindex;
    
    for(int i=0; i<nRow; i++)
    {
        for(int j=0; j<nCol; j++)
        {            
            sub2ind(nRow,cur_pixel,i,j);
            
            for (int m = 0; m < dims[2]; m++)
            {
                sumNeighbors = 0;
                for (int c=0; c<8; c++)
                {                
                  y = i + spoints[c+m*8][0]; 
                  x = j + spoints[c+m*8][1];

                  if(x <0 || x > nRow-1 || y <0 || y > nCol-1)
                     continue;  

                  //calculate floors, ceils and rounds for the x and y.
                  fy = floor(y); cy = ceil(y); ry = floor(y+0.5);
                  fx = floor(x); cx = ceil(x); rx = floor(x+0.5);

                  if ((abs(x - rx) < 1e-6) && (abs(y - ry) < 1e-6))
                  {
                      //interpolation is not needed, use original datatypes
                      sub2ind(nRow,index,y,x);
                      N = img[index];
                  }
                  else
                  {
                      //interpolation needed, use double type images
                      ty = y - fy;
                      tx = x - fx;

                      //calculate the interpolation weights
                      w1 = (1-tx)*(1-ty);
                      w2 =    tx *(1-ty);
                      w3 = (1-tx)*   ty ;
                      w4 =    tx *   ty ;

                      //compute interpolated pixel values
                      sub2ind(nRow,ffindex,fy,fx); sub2ind(nRow,fcindex,fy,cx);
                      sub2ind(nRow,cfindex,fy,fx); sub2ind(nRow,ccindex,cy,cx);

                      N = uint8(w1*img[ffindex] + w2*img[fcindex] + w3*img[cfindex] + w4*img[ccindex]);
                  }

                  if(N >= img[cur_pixel])
                      sumNeighbors++;

                  //left move by one bit
                  sumNeighbors = sumNeighbors<<1;
                }
            
                bnd[(j * nRow + i) * dims[2] + m] = sumNeighbors;
            }
        }
    }
}

