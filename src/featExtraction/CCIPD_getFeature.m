function fd = CCIPD_getFeature(featureName,img)
% img : image
% fd : 1D-vector of feature descriptor
br_th = 0.35; % blue ratio segmentation threshold
bg_mth = 220;
bg_sth = 30;
bg_cand = 15^2;


%br = mat2gray(img);
%br_th=mean(br(:))+2*std(br(:));
%br_th=mean(br(:))
%bin = br>br_th;
tbin = ones(size(img,1),size(img,2));
bin = logical(tbin);
%bin = (img/255)>br_th;
gray_img=double(rgb2gray(img));
cand_area = sum(bin(:)==1);

    switch featureName
       case 'ColorFeatures' % statistics (mean, std, median, mode) of color channels (RGB, HSV, HE, Lab,LUV, Br)
            fd = extract_color_features(img);
        case 'GeometricalFeatures' % statistics (mean, std, median, mode) of geometrical features
            fd = extract_geometrical_features(bin);
        case 'RegionalFeatures' %first-order regional features
           %
           regv=[];
           for i=1:size(img,3)
                imgi = img(:,:,i);
                v = extract_region_features(imgi,8);
                regv = [regv v];
           end
           fd = regv;
        case 'HOGHistogram' % histogram of HOG features
           %
           hogh=[];
           for i=1:size(img,3)
                imgi = img(:,:,i);
                statshog = [];
                hogmaps = vl_hog(im2single(imgi),3);
                for o=1:size(hogmaps,3)
                    hogmap = hogmaps(:,:,o);
                    h = hist(hogmap(:),8);
                    h = h./sum(h(:));
                    statshog = [statshog h];
                end
                hogh = [hogh statshog];
           end
           fd = hogh;
       case 'ShapeIndexHistogram' % histogram of ShapeIndex features
           %
           sih=[];
           for i=1:size(img,3)
               imgi = img(:,:,i);
               [h,~] = shapeindex_hit(imgi,8);
               v = h./sum(h(:));
               sih=[sih v];
           end
           fd = sih;
       case 'MultiscaleLBPFeatures' %statistics of multi-scale LBP features
           %
           mslbp =[];
           for i=1:size(img,3)
                imgi = img(:,:,i);
                statslbp = [];
                lbpDes = extract_lbp_features(imgi, 8, 3, 1);
                for s=1:length(lbpDes)
                    d = double(lbpDes{s});
                    h = hist(d(:),8);
                    h = h./sum(h(:));
                    statslbp = [statslbp h];
                end
                mslbp = [mslbp statslbp];
           end
           fd = mslbp;
       case 'LawFeatures' %statistics of law features
           %
            statslaw = [];
            for i=1:size(img,3)
                imgi = img(:,:,i);
                lawsfeats = lawsfilter(imgi); 
                for s=1:size(lawsfeats,3)
                    lawimg = lawsfeats(:,:,s);
                    h = hist(lawimg(:),8);
                    h = h./sum(h(:));
                    statslaw = [statslaw h];
                end
            end
            fd = statslaw;       
       case 'HaralickFeatures' % extract statistics of co-occurence matrices on grayscale image
          %if ((mean(gray_img(:))>bg_mth)&&(std(gray_img(:))<bg_sth))||(cand_area>bg_cand)
          %    fd = zeros(1,26);
          %else              
              %mask = logical(ones(size(img,1),size(img,2)));
              %mask(:,:,1)=bin;
              %mask(:,:,2)=bin;
              %mask(:,:,3)=bin;
              hf=[];
              for i=1:size(img,3)
                imgi = img(:,:,i);
                v = extract_haralick_features(cast(imgi,'uint16'),bin);
                hf = [hf v];
              end
              fd = hf;
          %end
       case 'GRMLFeatures' % extract statistics of run-length matrices on grayscale image
          statgrml = [];
          for i=1:size(img,3)
              imgi = img(:,:,i);
              v = extract_grml_features(imgi);
              statgrml = [statgrml v];
          end
           fd = statgrml;
       case 'GaborFeatures' % extract statistics of Gabor wavelets features
           %
           gf = [];
           for i=1:size(img,3)
               imgi = img(:,:,i);
               statgabor = [];
               gaborfeats = gabor_conv(imgi);
               size(gaborfeats)
               size(gaborfeats,3);
               for s=1:size(gaborfeats,3)
                  gaborimg = gaborfeats(:,:,s);
                  size(gaborimg);
                  size(bin);
                  %figure,imshow((gaborimg)/max(gaborimg(:)));
                  mgabor = mean(gaborimg(bin));
                  stdgabor = std(gaborimg(bin));
                  statgabor = [statgabor abs(mgabor) angle(mgabor) stdgabor];
               end
               gf=[gf statgabor];
           end
           fd = gf;
       case {'GraphFeatures'}
           % dispimgflag: declares whether or not the image should be displayed on the
        % screen (should be either 0 or 1)
        % annotate: declares whether or not the image should be annotated when it
        % is displayed (again, 0 or 1)        
          %if ((mean(gray_img(:))>bg_mth)&&(std(gray_img(:))<bg_sth))||(cand_area>bg_cand)
          %    fd = zeros(1,51);
          %else
            fd = graphfeats3(bin); % 51 graph features
          %end
       otherwise
          disp('Unknown CCIPD Feature');
          fd = [];
          return;
    end 
end 
