function img_out = gabor_conv(img)
%Image convolution with Gabor filter banks

%create gabor filter banks
%lambda  = [3 5 7];
lambda = 8;
theta   = 0;
psi     = [0 pi/2];
gamma   = 0.5;
bw      = 1;
N       = 8;

img_out = zeros(size(img,1), size(img,2), N*length(lambda));
for i=1:length(lambda)
    for n=1:N
        gb = gabor_fn(bw,gamma,psi(1),lambda(i),theta)...
            + 1i * gabor_fn(bw,gamma,psi(2),lambda(i),theta);
        % gb is the n-th gabor filter
        img_out(:,:,n) = imfilter(img, gb, 'symmetric');
        % filter output to the n-th channel
        theta = theta + 2*pi/N;
        % next orientation
    end
end

end 
