function clrspace_img = rgb2clrspace(rgb_img,clrspace)

if strcmp(clrspace,'RBrB')
    R=rgb_img(:,:,1);
    Br=uint8(blueratio(rgb_img));
    B=rgb_img(:,:,3);
    clrspace_img(:,:,1)=R;
    clrspace_img(:,:,2)=Br;
    clrspace_img(:,:,3)=B;
elseif strcmp(clrspace,'HSV') % too slow
    clrspace_img = rgb2hsv(rgb_img);
elseif strcmp(clrspace,'YIQ')
    clrspace_img = rgb2ntsc(rgb_img);    
elseif strcmp(clrspace,'YCbCr')
    clrspace_img = rgb2ycbcr(rgb_img);
elseif strcmp(clrspace,'Lab')
    clrspace_img = lab2uint8(rgb2lab(rgb_img));
elseif strcmp(clrspace,'HE2')    
    [ch1 ch2 ch3] = colour_deconvolution(rgb_img,'H&E 2');
    clrspace_img(:,:,1)=ch1;
    clrspace_img(:,:,2)=ch2;
    clrspace_img(:,:,3)=ch3;
elseif strcmp(clrspace,'HE')    
    [ch1 ch2 ch3] = colour_deconvolution(rgb_img,'H&E');
    clrspace_img(:,:,1)=ch1;
    clrspace_img(:,:,2)=ch2;
    clrspace_img(:,:,3)=ch3;    
elseif strcmp(clrspace,'BrEinv')    %blueratio and eosin invert
    [ch1 ch2 ch3] = colour_deconvolution(rgb_img,'H&E');
    Br=uint8(blueratio(rgb_img));
    clrspace_img(:,:,1)=Br;
    clrspace_img(:,:,2)=ch2;
    clrspace_img(:,:,3)=uint8(zeros(size(rgb_img,1),size(rgb_img,2)));
else % if no valid color space then returns original rgb image
    clrspace_img=rgb_img;
end
