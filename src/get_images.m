function [Timage, windImage] = get_images(path, time, level)
fT=fopen(fullfile(path, 'T', sprintf('T%d.dat', time)));
fU=fopen(fullfile(path, 'U', sprintf('U%d.dat', time)));
fV=fopen(fullfile(path, 'V', sprintf('V%d.dat', time)));
fW=fopen(fullfile(path, 'W', sprintf('W%d.dat', time)));
%%Prueba
%fT=fopen(sprintf('T%d.dat', time));
%fU=fopen(sprintf('U%d.dat', time));
%fV=fopen(sprintf('V%d.dat', time));
%fW=fopen(sprintf('W%d.dat', time));
%%

k=27;
FcomponenteT=fread(fT, (max(k))*280*(max(k))*287, 'float');
FcomponenteU=fread(fU, max(k)*281*max(k)*287, 'float');
FcomponenteV=fread(fV, max(k)*280*max(k)*288, 'float');
FcomponenteW=fread(fW, (max(k)+1)*280*(max(k)+1)*287, 'float');

g3f=zeros(287,280,27);
g4f=zeros(287,280,27);

g1f=zeros(281,287,27);
g2f=zeros(280,288,27);
gwfs=zeros(280,287,28);
% generar volumenes a traves de la información
for i=1:k
    g3 = reshape(FcomponenteT((i-1)*280*287+1:i*280*287), 280, 287);
    g4 = reshape(FcomponenteT((i-1)*280*287+1:i*280*287), 280, 287);
    mx=max(max(g3));
    mn=min(min(g3));
    
    g3=(g3-mn)/(mx-mn);
    g3=g3';
    g3=flipud(g3);
    g4=g4';
    g4=flipud(g4);
    g3f(:,:,i)=g3;
    g4f(:,:,i)=g4;
end
for i=1:k
    g = reshape(FcomponenteU((i-1)*281*287+1:i*281*287), 281, 287);
    g1= reshape(FcomponenteV((i-1)*280*288+1:i*280*288), 280, 288);
    %g1{i}=g1{i}';
    %g{i}=g{i}';
    g=flipud(g);
    g1=flipud(g1);
    g1f(:,:,i)=g;
    g2f(:,:,i)=g1;
end
for i=1:k+1
    g2 = reshape(FcomponenteW((i-1)*280*287+1:i*280*287), 280, 287);
    g2=flipud(g2);
    gwfs(:,:,i)=g2;
end
x=size(g);
y=size(g1);
z=size(g2);
U=zeros(z(1),z(2),k);
V=zeros(z(1),z(2),k);
W=zeros(z(1),z(2),k);
% Interpolar volumenes de Vientos [280,287,27]
for l=1:k
    for i=1:z(1)
        for j=1:z(2)
            U(i,j,l)=0.5*(g1f(i,j,l)+g1f(i+1,j,l));
        end
    end
end
for l=1:k
    for i=1:z(1)
        for j=1:z(2)
            V(i,j,l)=0.5*(g2f(i,j,l)+g2f(i,j+1,l));
        end
    end
end
for l=1:k
    for i=1:z(1)
        for j=1:z(2)
            W(i,j,l)=0.5*(gwfs(i,j,l)+gwfs(i,j,l+1));
        end
    end
end
% calcular campo vectorial y lineas de corriente
[X Y]=meshgrid(1:4:z(1),1:4:z(2));
[sx sy]=meshgrid(1:4:z(1),1:4:z(2));
%figure,imshow(rot90(flipud(U(:,:,10))),[]);colormap jet;hold on;
%figure,imshow(rot90(flipud(V(:,:,10))),[]);colormap jet;hold on;
%figure,imshow(rot90(flipud(W(:,:,10))),[]);colormap jet;hold on;
%figure,imshow(g3(:,:,1),[]);colormap jet;hold on;

windImage = zeros(287, 280, 3);
size(mat2gray(rot90(flipud(U(:,:,level)))));
windImage(:,:,1) = mat2gray(flipud(fliplr(U(:,:,level)')));

windImage(:,:,2) = mat2gray(flipud(fliplr(V(:,:,level)')));
windImage(:,:,3) = mat2gray(flipud(fliplr(W(:,:,level)')));
Timage = zeros(287, 280);
Timage = mat2gray(g3f(:,:,level));
fclose('all');
end
