clc;clear all;close all;
%Proceso de las anotaciones
%C1 para Vorticidad, C2 para Divergencia y C3 para Confluencia
filename='WRFdata/2005-04-29_00-00-00_climate_2014-09-29-JuanMolina-jmolinar3@ucentral.edu.co.wsa';
[vorticesP, divergenciasP, confluenciasP]=get_annotations(filename);


%Proceso de los metadatos
filename = 'metadataWRF.xml'
valores_long = get_metadata(filename, 'Longitude');
valores_lat = get_metadata(filename, 'Latitude');
valores_lvl = get_metadata(filename, 'Level');
longitudes = valores_long;
latitudes = valores_lat;
niveles = valores_lvl;
numlong = size(valores_long);
numlat = size(valores_lat);
numlvl = size(valores_lvl);
mapLong = containers.Map(longitudes,[1:numlong(1)]);
mapLat = containers.Map(latitudes,fliplr([1:numlat(1)]));
mapLvl = containers.Map(niveles,[1:numlvl(1)]);
latitudes = flipud(latitudes);
pruebalong = num2str(divergenciasP(2).x1)
pruebalat = num2str(divergenciasP(20).y1)
pruebalvl = num2str(vorticesP(5).nivel)
mapLong(str2num(pruebalong));
mapLat(str2num(pruebalat));
mapLvl(str2num(pruebalvl));


%Statistics generation
%Only XML annotations are used
listfiles= ['2003-04-29_00-00-00_climate_2014-09-29-JuanMolina-jmolinar3@ucentral.edu.co.wsa',
'2003-11-29_00-00-00_climate_2014-05-22-JuanMolina-jmolinar3@ucentral.edu.co.wsa',
'2004-04-29_00-00-00_climate_2014-09-29-JuanMolina-jmolinar3@ucentral.edu.co.wsa',
'2004-11-29_00-00-00_climate_2014-05-22-JuanMolina-jmolinar3@ucentral.edu.co.wsa',
'2005-04-29_00-00-00_climate_2014-09-29-JuanMolina-jmolinar3@ucentral.edu.co.wsa',
'2005-11-29_00-00-00_climate_2014-05-22-JuanMolina-jmolinar3@ucentral.edu.co.wsa'];
listmetadata = ['2003-04-29_00-00-00_climate_2014-09-29/metadataWRF.xml',
'2003-11-29_00-00-00_climate_2014-05-22/metadataWRF.xml',
'2004-04-29_00-00-00_climate_2014-09-29/metadataWRF.xml',
'2004-11-29_00-00-00_climate_2014-05-22/metadataWRF.xml',
'2005-04-29_00-00-00_climate_2014-09-29/metadataWRF.xml',
'2005-11-29_00-00-00_climate_2014-05-22/metadataWRF.xml'];
listfolders = ['2003-04-29_00-00-00_climate_2014-09-29',
'2003-11-29_00-00-00_climate_2014-05-22',
'2004-04-29_00-00-00_climate_2014-09-29',
'2004-11-29_00-00-00_climate_2014-05-22',
'2005-04-29_00-00-00_climate_2014-09-29',
'2005-11-29_00-00-00_climate_2014-05-22'];
z=size(listfiles)
annotations=cell(1,3)
for k=1:size(listfiles,1)
	filename = fullfile('WRFdata', listfiles(k,:));
	[vortices, divergencias, confluencias] = get_annotations(filename);
	disp(sprintf(filename));
	if size(vortices,2) > 1
		annotations{1} = [annotations{1} vortices];
	end
	if size(divergencias,2) > 1
		annotations{2} = [annotations{2} divergencias];
	end
	disp(sprintf('%d', size(confluencias,2)))
	if size(confluencias,2) > 1
		annotations{3} = [annotations{3} confluencias];
	end
end
volume(1).xmax = longitudes(end);
volume(1).xmin = longitudes(1);
volume(1).splits = numlong(1);
volume(2).xmax = latitudes(end);
volume(2).xmin = latitudes(1);
volume(2).splits = numlat(1);
volume(3).xmax = niveles(end);
volume(3).xmin = niveles(1);
volume(3).splits = numlvl(1);
[estadisticas, data] = gen_statistics(annotations, volume);

%
imgMC{1} = {'Imagen' 'Clase'};
imgroute{1} = {'Imagen' 'Clase'};
for k=1:size(listfiles,1) %Iterates over the number of XML annotations
	filename = fullfile('WRFdata', listfiles(k,:)); %Retrieves the metadata
	path = fullfile('WRFdata', listfolders(k,:)); %Retrieves the volume of data
	[vortices, divergencias, confluencias] = get_annotations(filename);
	disp(sprintf(filename));
	if size(vortices,2) > 1
		annotations{1} = [annotations{1} vortices];
		tmpsize = size(vortices);		
		for a=1:size(vortices, 2)
			tiempo = vortices(a).tiempo;
			nivel = vortices(a).nivel;
			[tImage, wImage] = get_images(path, tiempo, mapLvl(nivel));
			imgMC{end+1} = {getMulticlass(wImage, vortices(a), 70, 70, volume) 1};
			archivo = fullfile('datasetASAR', listfolders(k,:), sprintf('v%d.jpg', a));
			%archivow = fullfile('datasetASAROriginal', listfolders(k,:), sprintf('Wv%d.jpg', a));
			imwrite(cell2mat(imgMC{end}(1)), archivo);
			%imwrite(getboarderedimage(wImage, vortices(a), volume), archivow);
			imgroute{end+1} = {archivo 1};
		end
	end
	if size(divergencias,2) > 1
		annotations{2} = [annotations{2} divergencias];
		tmpsize = size(divergencias);		
		for a=1:size(divergencias,2)
			tiempo = divergencias(a).tiempo;
			nivel = divergencias(a).nivel;
			[tImage, wImage] = get_images(path, tiempo, mapLvl(nivel));
			imgMC{end+1} = {getMulticlass(wImage, divergencias(a), 70, 70, volume) 2};
			archivo = fullfile('datasetASAR', listfolders(k,:), sprintf('d%d.jpg', a));
			%archivow = fullfile('datasetASAROriginal', listfolders(k,:), sprintf('Wd%d.jpg', a));
			imwrite(cell2mat(imgMC{end}(1)), archivo);
			%imwrite(getboarderedimage(wImage, divergencias(a), volume), archivow);
			imgroute{end+1} = {archivo 2};
		end
	end
	disp(sprintf('%d', size(confluencias,2)))
	if size(confluencias,2) > 1
		annotations{3} = [annotations{3} confluencias];
		tmpsize = size(confluencias);		
		for a=1:size(confluencias,2)
			tiempo = confluencias(a).tiempo;
			nivel = confluencias(a).nivel;
			[tImage, wImage] = get_images(path, tiempo, mapLvl(nivel));
			imgMC{end+1} = {getMulticlass(wImage, confluencias(a), 70, 70, volume) 3};
			archivo = fullfile('datasetASAR', listfolders(k,:), sprintf('c%d.jpg', a));
			%archivow = fullfile('datasetASAROriginal', listfolders(k,:), sprintf('Wc%d.jpg', a));
			imwrite(cell2mat(imgMC{end}(1)), archivo);
			%imwrite(getboarderedimage(wImage, confluencias(a), volume), archivow);
			imgroute{end+1} = {archivo 3};
		end
	end
end

%Uncomment to generate the new dataset with the corresponding patches and file paths
%save('imgroute.mat', 'imgroute');
%save('imgMC.mat', 'imgMC');











%Below there is unfinished code
%imgesC{1} = {'Imagen' 'Clase'};
%imgesC{2} = {'Imagen' 'Clase'};
%imgesC{3} = {'Imagen' 'Clase'};
%imgesMC{1} = {'Imagen' 'Clase'};

%Procesar las anotaciones de acuerdo a su tiempo y nivel
for k=1:0
	filename = fullfile('WRFdata', listfiles(k,:));
	path = fullfile('WRFdata', listfolders(k,:))
	[vortices, divergencias, confluencias] = get_annotations(filename);
	disp(sprintf(filename));
	
	if size(vortices,2) > 1
		tmpsize = size(vortices)
		tmpalready(1).tiempo = 0
		tmpalready(1).nivel = 0
		for a=1:tmpsize(2)
			tiempo = vortices(a).tiempo;
			nivel = vortices(a).nivel;
			mapLvl(nivel);
			if isempty(find([tmpalready.tiempo] == tiempo & [tmpalready.nivel] == nivel))
				[tImage, wImage] = get_images(path, tiempo, mapLvl(nivel));
				tmpvortices = vortices(find([vortices.tiempo] == tiempo & [vortices.nivel] == nivel));
				[imgC, imgMC] = get_samples(tImage, wImage, tmpvortices, 70, 70, 35, 0.7, 'vortices', volume) %Oversampling en porcentaje
				imgesC{1} = [imgesC{1} imgC{:,2:end}]
				imgesMC{1} = [imgesMC{1} imgMC{:,2:end}]
				tmpalready(end+1)=struct('tiempo', tiempo, 'nivel', nivel)
			end
		end
	end
	if size(divergencias,2) > 1
		
	end
	disp(sprintf('%d', size(confluencias,2)))
	if size(confluencias,2) > 1
		
	else
		confluencias
	end
end



%Prueba Generación de las imágenes en RGB
%Se tratará cada anotación y se buscará el tiempo y el nivel, con lo cual se obtiene la imagen
%A su vez es necesario elaborar un conjunto de anotaciones a ese tiempo y nivel exclusivamente
%[tImage, wImage] = get_images(' ', 2 , mapLvl(str2num(pruebalvl)));
%imwrite(wImage, 'vientos.jpg');
%figure; imshow(tImage); colormap jet; hold on;
%figure; image(wImage); hold on;


