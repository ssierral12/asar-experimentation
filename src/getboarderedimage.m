function newImage = getborderedimage(wImage, anotaciones,volumen)
	x1=get_pixel(anotaciones.x1, volumen(1).xmax, volumen(1).xmin, volumen(1).splits)
	x2=get_pixel(anotaciones.x2, volumen(1).xmax, volumen(1).xmin, volumen(1).splits)
	y1=get_pixel(anotaciones.y1, volumen(2).xmax, volumen(2).xmin, volumen(2).splits)
	y2=get_pixel(anotaciones.y2, volumen(2).xmax, volumen(2).xmin, volumen(2).splits)
	newImage = wImage;
	newImage(x1:x2,y1,:)=0.0;
	newImage(x1:x2,y2,:)=0.0;
	newImage(x1,y1:y2,:)=0.0;
	newImage(x2,y1:y2,:)=0.0;
end

function pixel = get_pixel(xtmp, xmax, xmin, splits)
	pixel = floor(((xtmp-xmin)*splits)/(xmax-xmin));
	if pixel <= 0 
		pixel = 1
	elseif pixel>splits
		pixel = splits
	end
end
